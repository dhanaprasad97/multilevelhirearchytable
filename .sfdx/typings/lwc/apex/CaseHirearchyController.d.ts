declare module "@salesforce/apex/CaseHirearchyController.getChildCases" {
  export default function getChildCases(param: {caseId: any}): Promise<any>;
}
