import { LightningElement, track, api, wire } from 'lwc';
import getChildCases from '@salesforce/apex/CaseHirearchyController.getChildCases';
import No_Child_Case_Records from '@salesforce/label/c.No_Child_Case_Records';
import { NavigationMixin } from 'lightning/navigation';

export default class LwcCaseHirerachy extends NavigationMixin(LightningElement) {

    // api variables
    @api recordId;
    @api isChildComponent = false;

    // track variables
    @track cases = [];

    showSpinner = true;
    error;
    label = {
        noChildCases : No_Child_Case_Records
    }

    // wire method to get child cases
    @wire(getChildCases, { caseId: '$recordId' })
    wiredCases({ error, data }) {
        this.cases = [];
        console.log('data', JSON.stringify(data));
        console.log('error', JSON.stringify(error));
        if (data) {
            for(let element of data){
                let _element =  Object.assign({},element);
                _element.isExpand =  false; // assigning property for expand or collapse
                this.cases.push(_element);
            }
            this.error = undefined;
            this.showSpinner = false;
        } else if (error) {
            this.error = JSON.stringify(error);
            this.cases = [];
            this.showSpinner = false;
        }
    }

    get  hasChildCases(){
        return this.cases &&  this.cases && this.cases.length>0;
    }

    // expand collapse of case related cases
    handleChildCaseComponent(event){
        let index =  event.currentTarget.dataset.index;
        this.cases[index].isExpand = !(this.cases[index].isExpand);
    }

    //navigating to case on click of case number using navigation mixin
    handleNavigateToCase(event){
        let index =  event.currentTarget.dataset.index;
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.cases[index].Id,
                actionName: 'view'
            }
        });  
    }

}