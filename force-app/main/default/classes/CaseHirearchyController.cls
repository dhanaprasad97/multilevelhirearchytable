/**
 * Created by : Dhana Prasad
 * Description : used to query related cases of case
 */
public with sharing class CaseHirearchyController {
   
    /**Method Name : getChildCases
     * Description : queries and returns child cases of a case
     * params : Id caseId
     * return : List<Case> 
     */
    @AuraEnabled(cacheable=true)
    public static List<Case> getChildCases(Id caseId){
          return  [SELECT Id, CaseNumber, Subject, Origin FROM Case 
                    WHERE ParentId =: caseId  WITH SECURITY_ENFORCED ORDER BY CaseNumber DESC ];  
    }
}