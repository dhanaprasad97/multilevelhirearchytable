/**Class Name :  CaseHirearchyController_Test
 * Test class for controller : CaseHirearchyController
 * created by : Dhana Prasad
*/
@isTest
public class CaseHirearchyController_Test {

    /**Method : makeData 
     * Description : this is used to prepare data according to controller.
    */
    @TestSetup
    static void makeData(){
        List<Case> caseList =  TestDataFactory.createCaseRecords('case hirerachy',5, true);
        if(!caseList.isEmpty()){
            // making records for case hirerachy
            for(Integer i = 1; i< caseList.size() ; i++){
                caseList[i].ParentId = caseList[0].Id;
            }
            update caseList;
        }
    }

    /**Method : testGetChildCases 
     * Description : this is used to test method getChildCases of controller with proper input
    */
    @isTest
    public static void testGetChildCases(){
        List<Case> caseList = [SELECT Id FROM Case WHERE ParentId = null]; // querying parent case
        Test.startTest();
            List<case> childCaseList  = CaseHirearchyController.getChildCases(caseList[0].Id);
            System.assertEquals(4, childCaseList.size());
        Test.stopTest();
    }
  
}